import { getFighterInfo } from "../fighterSelector";

export function showWinnerModal(fighter) {
  // call showModal function 
  const title = "Winner";
  const bodyElement = createFighterDetails(fighter);
  showModal({ title, bodyElement });
}

function createFighterDetails(fighter) {
  const { name } = fighter;

  const getFighterInfo = createElement({
    tagName: "div",
    className: "modal-body",
  });
  const nameElement = createElement({
    tagName: "span",
    className: "fighter-name",
  });

  nameElement.innerText = name;
  getFighterInfo.append(nameElement);

  return getFighterInfo;

}
