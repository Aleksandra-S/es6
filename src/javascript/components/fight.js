import { controls } from '../../constants/controls';

export async function fight(firstFighter, secondFighter) {
  return new Promise((resolve) => {
    // resolve the promise with the winner when fight is over

  const fightOver = () => firstFighter.health <= 0 || secondFighter.health <= 0;

  const punch = (first, second) => (second.health -= getDamage(first, second));

  const nextRound = (first, second) => punch(second, first);

  let winner;

  punch(firstFighter, secondFighter);

  while (!fightOver()) {
    punch(firstFighter, secondFighter);
    if (!fightOver()) {
      nextRound(firstFighter, secondFighter);
    }
  }

  winner = firstFighter.health > 0 ? firstFighter : secondFighter;

  return winner;
  }

  });
}

export function getDamage(attacker, defender) {
  // return damage
  return Math.max(getHitPower(attacker)-getBlockPower(defender), 0);
  }

export function getHitPower(fighter) {
  // return hit power
  return fighter.attack * criticalHitChance();
}

export function getBlockPower(fighter) {
  // return block power
  return fighter.defence * dodgeChance();
  }
  function criticalHitChance(){
    return Math.random() + 1;
  }

  function dodgeChance(){
    return Math.random() + 1;
  }
